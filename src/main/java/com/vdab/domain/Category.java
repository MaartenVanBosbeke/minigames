package com.vdab.domain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString(callSuper = true)

public class Category extends BaseEntity{

    private String categoryName;

}
