package com.vdab.domain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString(callSuper = true)

public class Borrower extends BaseEntity{

    private String borrowerName;
    private String street;
    private String houseNumber;
    private String busNumber;
    private String postalCode;
    private String city;
    private String telephone;
    private String email;



}
