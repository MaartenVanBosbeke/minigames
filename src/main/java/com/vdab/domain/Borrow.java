package com.vdab.domain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@SuperBuilder
@ToString(callSuper = true)

public class Borrow extends BaseEntity{

    private Game game;
    private Borrower borrower;
    private LocalDate borrowDate;
    private LocalDate returnDate;

}
