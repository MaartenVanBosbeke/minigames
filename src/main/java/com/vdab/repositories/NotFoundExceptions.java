package com.vdab.repositories;

public class NotFoundExceptions extends Throwable{

    public NotFoundExceptions(String message) {
        super(message);
    }
}
