package com.vdab.repositories;

import com.vdab.domain.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BorrowRepository {

    public List<Borrow> findAllBorrowedGames() throws NotFoundExceptions {
        try (Connection myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) { //STEP3 and STEP7 Try-With-Resource makes it possible to autoclose the connection
            PreparedStatement statement = myConnection.prepareStatement("select * from borrow");
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            List<Borrow> borrowList = new ArrayList<>();
            while (resultSet.next()) {
                Date borrowDate = resultSet.getDate("borrow_date");
                Date returnDate = resultSet.getDate("return_date");

                Borrow borrow = Borrow.builder()
                        .id(resultSet.getInt("id"))
                        .borrowDate(borrowDate != null ? borrowDate.toLocalDate() : null)
                        .returnDate(returnDate != null ? returnDate.toLocalDate() : null)
                        .build();
                borrowList.add(borrow);
            }
            return borrowList;

        } catch (SQLException e) {
            throw new NotFoundExceptions("borrowRepo - findAllBorrowedGames() - no games found");
        }
    }

    public List<Borrow> searchByBorrowedLongerThanXDays(int choice) throws NotFoundExceptions {

        try (Connection myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = myConnection.prepareStatement("SELECT * FROM borrow as b inner join game as g on b.game_id = g.id inner join borrower as ber on b.borrower_id=ber.id where datediff('2021-05-03 12:00:00', b.borrow_date) >= ?");
            statement.setInt(1, choice);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            List<Borrow> borrowList = new ArrayList<>();

            while (resultSet.next()) {
                Date returnDate = resultSet.getDate("return_date");
                if(returnDate == null) {
                    Borrow borrow = borrowBorrowerGamesBuilder(resultSet);
                    borrowList.add(borrow);
                }
            }
            return borrowList;

        } catch (SQLException e) {
            throw new NotFoundExceptions("borrowRepo - searchByBorrowedLongerThanXDays() - no games found");
        }
    }

    private Borrow borrowBorrowerGamesBuilder(ResultSet resultSet) throws SQLException {

        //SELECT * FROM borrow as b inner join game as g on b.game_id = g.id inner join borrower as ber on b.borrower_id=ber.id
        Date borrowDate = resultSet.getDate("b.borrow_date");
        Date returnDate = resultSet.getDate("b.return_date");

        return Borrow.builder()
                .id(resultSet.getInt("b.id"))
                .game(Game.builder()
                        .id(resultSet.getInt("g.id"))
                        .gameName(resultSet.getString("g.game_name"))
                        .editor(resultSet.getString("g.editor"))
                        .author(resultSet.getString("g.author"))
                        .yearEdition(resultSet.getInt("g.year_edition"))
                        .age(resultSet.getString("g.age"))
                        .minPlayers(resultSet.getInt("g.min_players"))
                        .maxPlayers(resultSet.getInt("g.max_players"))
                        .playDuration(resultSet.getString("g.play_duration"))
                        .price(resultSet.getDouble("g.price"))
                        .image(resultSet.getString("g.image"))
                        .build())
                .borrower(Borrower.builder()
                        .borrowerName(resultSet.getString("ber.borrower_name"))
                        .street(resultSet.getString("ber.street"))
                        .houseNumber(resultSet.getString("ber.house_number"))
                        .busNumber(resultSet.getString("ber.bus_number"))
                        .postalCode(resultSet.getString("ber.postcode"))
                        .city(resultSet.getString("ber.city"))
                        .telephone(resultSet.getString("ber.telephone"))
                        .email(resultSet.getString("ber.email"))
                        .build())
                .borrowDate(borrowDate != null ? borrowDate.toLocalDate() : null)
                .returnDate(returnDate != null ? returnDate.toLocalDate() : null)
                .build();

    }
}
