package com.vdab.repositories;

import com.vdab.domain.Category;
import com.vdab.domain.Difficulty;
import com.vdab.domain.Game;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameRepository {


    public Game findById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id=c.id inner join difficulty as d on g.difficulty_id=d.id where g.id = ?");
            statement.setInt(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

            return gamesBuilder(resultSet);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public Game findGameByName(String name) throws NotFoundExceptions {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id=c.id inner join difficulty as d on g.difficulty_id=d.id where game_name LIKE ?"); //LIKE: lowercase
            statement.setString(1, "%" + name + "%");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

            return gamesBuilder(resultSet);

        } catch (SQLException throwables) {
            throw new NotFoundExceptions("gameRepo - findByGameName - Game not found");
        }
    }

    private Game gamesBuilder(ResultSet resultSet) throws SQLException { //method only for our builder, needed in multiple methods

        return Game.builder()
                .id(resultSet.getInt("g.id"))
                .gameName(resultSet.getString("game_name"))
                .editor(resultSet.getString("editor"))
                .author(resultSet.getString("author"))
                .yearEdition(resultSet.getInt("year_edition"))
                .age(resultSet.getString("age"))
                .minPlayers(resultSet.getInt("min_players"))
                .maxPlayers(resultSet.getInt("max_players"))
                .category(Category.builder()
                        .id(resultSet.getInt("c.id"))
                        .categoryName(resultSet.getString("category_name"))
                        .build())
                .playDuration(resultSet.getString("play_duration"))
                .difficulty(Difficulty.builder()
                        .id(resultSet.getInt("d.id"))
                        .difficultyName(resultSet.getString("difficulty_name"))
                        .build())
                .price(resultSet.getDouble("price"))
                .image(resultSet.getString("image"))
                .build(); //category must be returned to the service, and then to the main
    }

    public List<Game> findAll() throws NotFoundExceptions {
        try (Connection myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) { //STEP3 and STEP7 Try-With-Resource makes it possible to autoclose the connection
            PreparedStatement statement = myConnection.prepareStatement("select * from game as g inner join category as c on g.category_id=c.id inner join difficulty as d on g.difficulty_id=d.id order by g.game_name");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            List<Game> gameList = new ArrayList<>();
            while (resultSet.next()) {
                Game game = gamesBuilder(resultSet);
                gameList.add(game);
            }
            return gameList;

        } catch (SQLException e) {
            throw new NotFoundExceptions("gameRepo - findAll - no games found");
        }
    }

    public Game findByBorrowId(int id){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id=c.id inner join difficulty as d on g.difficulty_id=d.id inner join borrow as b on g.id=b.game_id where b.id = ?");
            statement.setInt(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

            return gamesBuilder(resultSet);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public List<Difficulty> showDifficultyList() throws NotFoundExceptions {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from difficulty ");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            List<Difficulty> difficultyList = new ArrayList<>();
            while (resultSet.next()) {
                Difficulty difficulty = Difficulty.builder()
                        .id(resultSet.getInt("id"))
                        .difficultyName(resultSet.getString("difficulty_name"))
                        .build();
                difficultyList.add(difficulty);
            }
            return difficultyList;

        } catch (SQLException throwables) {
            throw new NotFoundExceptions("gameRepo - showDifficultyList() - error");
        }
    }

    public List<Game> showGameListByChosenDifficulty(int choice) throws NotFoundExceptions {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM game as g inner join difficulty as d on g.difficulty_id=d.id inner join category as c on g.category_id=c.id where d.id >= ?");
            statement.setInt(1, choice);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            List<Game> gameListByChosenDifficulty = new ArrayList<>();
            while (resultSet.next()) {
                Game game = gamesBuilder(resultSet);
                gameListByChosenDifficulty.add(game);
            }
            return gameListByChosenDifficulty;

        } catch (SQLException throwables) {
            throw new NotFoundExceptions("gameRepo - showGameListByChosenDifficulty() - error");
        }
    }

    private static String createQueryforGameAndChosenDifficulty(int length) {
        PreparedStatement statement;
        String query = "select * from difficulty as d inner join game as g on d.id = g.difficulty_id where in (";
        StringBuilder queryBuilder = new StringBuilder(query);
        for (int i = (5-length); i < length; i++) {
            queryBuilder.append(" ?");
            if (i != length - 1)
                queryBuilder.append(",");
        }
        queryBuilder.append(")");
        return queryBuilder.toString();

    }
}
