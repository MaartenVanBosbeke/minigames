package com.vdab.repositories;


import com.vdab.domain.Category;

import java.sql.*;

public class CategoryRepository {

    public Category findById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from category where id = ?");
            statement.setInt(1, id);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

//            Category category = Category.builder()
//                    .categoryName(resultSet.getString("category_name"))
//                    .build();

            return Category.builder()
                    .categoryName(resultSet.getString("category_name"))
                    .build(); //category must be returned to the service, and then to the main

        } catch (SQLException throwables) {

            throwables.printStackTrace();
            return null;
        }
    }
}
