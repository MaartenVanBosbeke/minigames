package com.vdab.repositories;

import com.vdab.domain.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BorrowerRepository {
    public Borrower findById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from borrower where id = ?");
            statement.setInt(1, id);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

            return borrowerBuilder(resultSet);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public Borrower findByBorrowId(int id){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from borrower inner join borrow on borrower.id=borrow.borrower_id where borrow.id = ?");
            statement.setInt(1, id);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();
            resultSet.next();

            return borrowerBuilder(resultSet);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

    }

    public List<Borrower> searchByBorrowerName(String name) throws NotFoundExceptions {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from borrower where borrower_name LIKE ? order by borrower_name");
            statement.setString(1, "%" + name + "%");
            statement.execute();

            ResultSet resultSet = statement.getResultSet();
            List<Borrower> borrowerList = new ArrayList<Borrower>();
            while(resultSet.next()){
                Borrower b = borrowerBuilder(resultSet);
                borrowerList.add(b);
            }


            return borrowerList;

        } catch (SQLException throwables) {
            throw new NotFoundExceptions("gameRepo - showGameListByChosenDifficulty() - error");
        }
    }

    public Borrower borrowerBuilder(ResultSet resultSet) throws SQLException {
        return Borrower.builder()
                .borrowerName(resultSet.getString("borrower_name"))
                .street(resultSet.getString("street"))
                .houseNumber(resultSet.getString("house_number"))
                .busNumber(resultSet.getString("bus_number"))
                .postalCode(resultSet.getString("postcode"))
                .city(resultSet.getString("city"))
                .telephone(resultSet.getString("telephone"))
                .email(resultSet.getString("email"))
                .build();
    }


}
