package com.vdab;

import com.vdab.domain.*;
import com.vdab.repositories.NotFoundExceptions;
import com.vdab.services.BorrowService;
import com.vdab.services.BorrowerService;
import com.vdab.services.CategoryService;
import com.vdab.services.GameService;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    private static Scanner scan = new Scanner(System.in);
    private static CategoryService categoryService = new CategoryService();
    private static GameService gameService = new GameService();
    private static BorrowerService borrowerService = new BorrowerService();
    private static BorrowService borrowService = new BorrowService();


    public static void main(String[] args) {

        System.out.println("Welcome to our games");
        showAndChooseInitialOptions();

    }

    public static void showAndChooseInitialOptions() {
        showInitialOptions();
        chooseInitialOptions();
    }

    public static void showInitialOptions() {
        System.out.println("What do you want to do?" +
                "\n\t1. Show the first game category" +
                "\n\t2. Show the fifth game" +
                "\n\t3. Show the first borrower" +
                "\n\t4. Show a game of your choice" +
                "\n\t5. Show all games" +
                "\n\t6. Show a list and choose a game" +
                "\n\t7. Show borrowed games" +
                "\n\t8. Advanced search: difficulty" +
                "\n\t9. Complex search: borrowers" +
                "\n\t10. Advanced search: search for borrowed games longer than X days" +
                "\nEnter your choice: ");
    }

    public static void chooseInitialOptions(){

        int choice = scan.nextInt();
        switch (choice) {
            case 1:
                showFirstGameCategory();
                showAndChooseInitialOptions();
                break;
            case 2:
                showFifthGame();
                showAndChooseInitialOptions();
                break;
            case 3:
                showFirstBorrower();
                showAndChooseInitialOptions();
                break;
            case 4:
                findGameByName();
                showAndChooseInitialOptions();
                break;
            case 5:
                showAllGames();
                showAndChooseInitialOptions();
                break;
            case 6:
                showListOfGamesWithNameAndCategory();
                showGameAndCategoryByGameName();
                showAndChooseInitialOptions();
                break;
            case 7:
                showBorrowedGames();
                showAndChooseInitialOptions();
                break;
            case 8:


            default:
                System.out.println("init options default - Wrong input, try again. " +
                        "\nRedirecting to main menu");
                showAndChooseInitialOptions();
        }
    }

    private static void searchByBorrowedLongerThanXDays() {
        /*
        show borrowed games that have been lent out longer that x amount of days, where x is user input
        show name of borrower, show name of game, show borrowdate
         */
        System.out.println("Enter a number of days, to search for games that have been lent out longer and have not yet been returned:");
        int choice = scan.nextInt();
        try {
            borrowService.searchByBorrowedLongerThanXDays(choice).stream().forEach(game -> System.out.format("BorrowerName: %-50s | GameName: %-50s | Borrowed since: %-20s %n", game.getBorrower().getBorrowerName(), game.getGame().getGameName(), game.getBorrowDate()));
        }catch(NotFoundExceptions notFoundExceptions){
            notFoundExceptions.printStackTrace();
        }
    }

    private static void searchByBorrowerName() {
        System.out.println("Type the name of the borrower you want to search for: ");
        String name = scan.next();
        try{
            borrowerService.searchByBorrowerName(name).stream().forEach(borrower -> System.out.println(
                    "Name: " + borrower.getBorrowerName() + ", city: " + borrower.getCity() +
                    ", telephone" + borrower.getTelephone() + ", email: " + borrower.getEmail()));
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions){
            notFoundExceptions.printStackTrace();
        }

    }

    private static void showListOfAllDifficulties() {
        try {
            System.out.println("All difficulties: ");
            List<Difficulty> difficultyList = gameService.showDifficultyList();
            difficultyList.forEach(difficulty -> System.out.println("Difficulty level: " + difficulty.getId() + ", difficulty name: " + difficulty.getDifficultyName()));
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }
    }

    private static void chooseDifficultyAndUp() {
        try {
            System.out.println("Enter the difficulty level");
            int choice = scan.nextInt();
            System.out.println("Every difficulty from " + choice + " and up: ");
            List<Difficulty> filteredDifficultyList = gameService.showDifficultyList().stream().filter(difficulty -> difficulty.getId() >= choice).collect(Collectors.toList()); //scan is equal or larger than difficulty.getId, cl-ollect those and add them to List() filteredDifList;
            filteredDifficultyList.stream().forEach(difficulty -> System.out.println("\t" + difficulty.getId() + ". " + difficulty.getDifficultyName()));
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }
    }

    private static void showGameListByChosenDifficulty() {
        System.out.println("Enter the difficulty level");
        int choice = scan.nextInt();
        System.out.println("Every game with difficulty from " + choice + " and up: ");
        try {
            gameService.showGameListByChosenDifficulty(choice).stream().forEach(g -> System.out.println(g.getGameName() + " " + g.getEditor() +
                    " " + g.getPrice() + " " + g.getDifficulty().getDifficultyName()));
//            List<Game> filteredList = gameService.showGameListByChosenDifficulty(choice);
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }

    }

    private static void showBorrowedGames() {
        try {
            borrowService.findAllBorrowedGames().forEach(borrow -> System.out.format("Game name: %-50s | BorrowerName: %-50s | BorrowDate: %tD | ReturnDate: %tD %n", borrow.getGame().getGameName(), borrow.getBorrower().getBorrowerName(), borrow.getBorrowDate(), borrow.getReturnDate()));
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }
    }

    private static void showListOfGamesWithNameAndCategory() {
        try {
            gameService.findAll().stream().forEach(game -> System.out.format("Name: %-50s| Category: %-55s %n", game.getGameName(), game.getCategory().getCategoryName()));
//            List<Game> gameList = gameService.findAll();
//            for(Game game : gameList){
//                System.out.format("Name: %-50s| Category: %-55s %n", game.getGameName(), game.getCategory().getCategoryName());
//                //- => left allign; 50 => max chars; .2 => 2 digits after comma; f => floating point (!float); %n => newLine
//            }
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }
    }

    private static void showGameAndCategoryByGameName() {
        System.out.println("Type the name of the game:");
        String name = scan.next();
        try {
            Game game = gameService.findGameByName(name);
            System.out.println(game);
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            System.out.println("main, showGameAndCategoryByGameName - Exception game not found. " + notFoundExceptions.getMessage());
//            notFoundExceptions.printStackTrace(); shows details about exception
        }

    }

    private static void showAllGames() {
        try {
            List<Game> gameList = gameService.findAll();
            for (Game game : gameList) {
                System.out.format("Name: %-50s| Editor: %-55s| Price: %.2f %n", game.getGameName(), game.getEditor(), game.getPrice());
                //- => left allign; 50 => max chars; .2 => 2 digits after comma; f => floating point (!float); %n => newLine
//                System.out.println("Name: " + game.getGameName() + ", editor: " + game.getEditor() + ", price" + game.getPrice());
            }
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            notFoundExceptions.printStackTrace();
        }
    }

    private static void findGameByName() {
        System.out.println("Type the name of the game:");
        String name = scan.next();
        try {
            Game game = gameService.findGameByName(name);
            System.out.println("Name: " + game.getGameName() + ", publisher: " + game.getEditor() +
                    ", age: " + game.getAge() + ", price: " + game.getPrice());
            System.out.println("**********");
        } catch (NotFoundExceptions notFoundExceptions) {
            System.out.println("main, findGameByName - Exception game not found. " + notFoundExceptions.getMessage());
//            notFoundExceptions.printStackTrace(); shows details about exception
        }
    }

    private static void showFirstBorrower() {
        Borrower borrower = borrowerService.findById(1);
        System.out.println("First borrower: ");
        System.out.println("borrower name: " + borrower.getBorrowerName() + ", city: " + borrower.getCity());
        System.out.println("*****************");

//        System.out.println("First borrower (full): ");
//        System.out.println(borrower);
//        System.out.println("*****************");
    }

    private static void showFifthGame() {
        Game game = gameService.findById(5);
        System.out.println("Fifth game: ");
        System.out.println(game);
        System.out.println("*****************");
    }

    private static void showFirstGameCategory() {
        Category category = categoryService.findById(1); //TODO: Initiate categoryService + print out the catergory
        System.out.println("first category: ");
        System.out.println(category);
        System.out.println("*****************");
    }

}

/*there are 4 types of how you can create a method:
    1. public void methodName(){}  ---> method that returns nothing (void) and receives nothing
    2. public void methodName(TypeOfParameter parameter){} ---> method that returns nothing (void) but receives something (TypeOfParameter parameter)
    3. public ReturnType methodName(){} ---> method that returns something (ReturnType is the type of what is being returned) but receives nothing ()
    4. public ReturnType methodName(TypeOfParameter parameter){} ---> method that returns something and receives something (TypeOfParameter parameter)
    5. public ReturnType methodeName(TypeOfParameter parameter) throws AnException {} ---> this is a method that can throw an Exception
*/