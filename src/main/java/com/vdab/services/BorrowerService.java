package com.vdab.services;

import com.vdab.domain.Borrower;
import com.vdab.repositories.BorrowerRepository;
import com.vdab.repositories.NotFoundExceptions;

import java.util.Arrays;
import java.util.List;

public class BorrowerService {

    private static BorrowerRepository borrowerRepository = new BorrowerRepository();

    public Borrower findById(int id) {
        return borrowerRepository.findById(id);
    }

    public List<Borrower> searchByBorrowerName(String name) throws NotFoundExceptions {
        return borrowerRepository.searchByBorrowerName(name);
    }


}
