package com.vdab.services;

import com.vdab.domain.Category;
import com.vdab.repositories.CategoryRepository;

public class CategoryService {

    private CategoryRepository categoryRepository = new CategoryRepository();

    public Category findById(int id) {
        return categoryRepository.findById(id);
    }

}
