package com.vdab.services;

import com.vdab.domain.Difficulty;
import com.vdab.domain.Game;
import com.vdab.repositories.GameRepository;
import com.vdab.repositories.NotFoundExceptions;

import java.util.List;

public class GameService {

    private GameRepository gameRepository = new GameRepository();

    public Game findById(int id) {
        return gameRepository.findById(id);
    }

    public Game findGameByName(String name) throws NotFoundExceptions {
        return gameRepository.findGameByName(name);
    }

    public List<Game> findAll() throws NotFoundExceptions {
        return gameRepository.findAll();
    }

    public List<Difficulty> showDifficultyList() throws NotFoundExceptions {
        return gameRepository.showDifficultyList();
    }

    public List<Game> showGameListByChosenDifficulty(int choice) throws NotFoundExceptions {
        return gameRepository.showGameListByChosenDifficulty(choice);
    }
}
