package com.vdab.services;

import com.vdab.domain.Borrow;
import com.vdab.repositories.BorrowRepository;
import com.vdab.repositories.BorrowerRepository;
import com.vdab.repositories.GameRepository;
import com.vdab.repositories.NotFoundExceptions;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BorrowService {

    private BorrowRepository borrowRepository = new BorrowRepository();
    private GameRepository gameRepository = new GameRepository();
    private BorrowerRepository borrowerRepository = new BorrowerRepository();

    public List<Borrow> findAllBorrowedGames() throws NotFoundExceptions {
        List<Borrow> borrowList = borrowRepository.findAllBorrowedGames();
        for (Borrow borrow : borrowList) {
            borrow.setGame(gameRepository.findByBorrowId(borrow.getId()));
            borrow.setBorrower(borrowerRepository.findByBorrowId(borrow.getId()));
        }
        Collections.sort(borrowList, Comparator.comparing(o -> o.getBorrower().getBorrowerName()));
        Collections.sort(borrowList, Comparator.comparing(Borrow::getBorrowDate));
        return borrowList;
    }

    public List<Borrow> searchByBorrowedLongerThanXDays(int choice) throws NotFoundExceptions {
        return borrowRepository.searchByBorrowedLongerThanXDays(choice);
    }


}
